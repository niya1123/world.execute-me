# Mili - world.execute(me);

## 非公式のものです。問題があれば消します。

Mili - world.execute(me);のコードを実際に書き起こしたもです。皆さんにこの素晴らしい曲のコード的な意味も付け加えればより一層この曲が好きになると思い、書き起こしました。コードにも著作権があるので引用という形を足らせていただきたいと思いますが、何かしら問題が発生すれば即座に削除等の対策を取らせていただきますので、ご理解お願いいたします。

素晴らし本家様　→　[ Mili - world.execute(me);](https://www.youtube.com/watch?v=ESx_hy1n7HA)


## 考察その１

それでは早速考察していきたいと思います。まず初めにご覧いただくべき場所はコメントアウトからですね。

> The program GodDrinksJava implements an application that
creates an empty simulated world with no meaning or purpose.

これをgoogle翻訳さんで訳すと以下のようになります。

` プログラムGodDrinksJavaは、意味や目的がない空のシミュレートされた世界を作り出します。 `

これから察するに、このプログラム（世界観？）としては、本当に世界の空の箱庭を作っているイメージでしょうか。真っ白な白い箱の世界を作るプログラムなのだろうと思います。

さてここでGodDrinksJavaというファイル名にも注目しますと、単純に読めば　
` 神はJavaを飲む `　
という一見よくわからない意味になります。

ここでいう神をいうのは所謂このプログラムを書いた人物＝プログラマというのは予想がつきます。

では、drinksとは何なのか。ここでもgoogleさんを使って訳を探してみると一番しっくり来たのは、召すという意味でした。

**`神はJavaを召す`**　…よくわからないですね。しかし、召すをcall若しくはtakeと考えるとこのプログラムに対してjavaという言語の手段をとった。というような意味になるのかなと自分は思いました。

次に行ってみましょう。このクラスは単一クラスで、mainメソッドを持ったプログラムだということがとりあえずわかります。

次からは少々プログラム的な考え方から考察していきます。
まず初めに

```java
Thing you = new Lovable("Me", 0, true, -1, false);
Thing me = new Lovable("You", 0, false, -1, false);
```

の部分から。

Thing型の変数名you,meというLovableオブジェクトに対して適切な引数を与えて初期化しています。

引数としては、String,int,bool,int,boolが与えられています。これが指し示すことは今のところはあまり思いつきません。が、後々を見てみると、Me,You = プログラム自身（擬人化させると）, プログラマという構図が成り立つようには思えます。

このことを念頭に置いたうえでそれぞれのクラスの名前を見てみましょう。

まず、Thingは親クラスでLovableが子クラスということは分かるでしょう。これはThing=物クラスすなわち有機物、無機物含むすべての物クラスであり、Lovableクラスはその中でも愛らしい=人間若しくは有機物の何かというのは予想がつきます。

ではこのLovable=愛らしいというのは誰目線からなのか。これはプログラマー目線でしょう。なぜならここで先ほど考えなかった引数に関して考えてみましょう。　**<font color = "green">Me</font>** と **`You`** に違いがある場所は第２引数のboolです。ここではMeがtrue, Youがfalseとなっています。そして先ほど述べた有機物、無機物の分別なのではないかと考えられます。

この時点ではまだ、trueが有機物なのか無機物なのかは分からないので次に進みます。

次はWorldクラスの変数名worldオブジェクトを生成しています。引数にはintの5が与えられていますが、現段階ではこれが何を示しているのかはわかりません。

次に、
```java
world.addThing(me);

world.addThing(you);
```
を見ると、作られたworldオブジェクトに対してme,youを追加しています。

これで初期化された世界に私とあなたが追加されました。

そうして
```java
world.startSimulation();
```
世界はシュミレートされます。


## 考察その２

さて、続いてはif分の部分です。
```java
if(me instanceof PointSet){
    you.addAttribute(me.getDimensions().toAttribute());
     me.resetDimensions();
}
```

まずはこれですね。まずif分の条件ですが、meがPointSetと同じインスタンスならば処理を実行します。ここで言うPointSetと言うのは座標設定でしょうか。

処理内容としてはyouに対してaddAttributeすなわち属性を付与しています。そして、その属性というのはmeのgetDimensions直訳で寸法をtoAttributeすなわち、属性に変換してyouに付与しています。

その次に,me.resetDimensionsすなわち寸法をリセットしています。（歌詞的に考えると0=君に捧げたと言えるでしょう）

続いて2つめのif文を見てみましょう。
```java
if(me instanceof Circle){
    you.addAttribute(me.getCircumference().toAttribute());
    me.resetCircumference();
}
```
条件としてはmeのインスタンスがCircleのインスタンスと一致していれば、先ほどと同じように属性（Circumference:円周）をyouに追加しています。そしてmeのの円周を初期化しています。

続いて3つ目のif文です。

```java
if(me instanceof SineWave){
    you.addAction("sit", me.getTangent(you.getXPosition()));
}
```
meがSinWave(正弦波)ならyouに行動を追加しています。どういう行動かというと、第一引数の`"sit"`と第二引数の`me.getTangent(you.getXPosition())`から、meの正弦とyのx座標が交差する地点に座ってくださいという行動をしろということだと思われます。歌詞からもこれらは考え的に正しいと思われます。

４つ目のif文です。

```java
if(me instanceof Sequence){
    me.setLimit(you.toLimit());
}
```

meがSequence(数列)ならyouはtoLimitすなわち極限に近づき、meのsetLimitすなわち極限にsetされると読み取れます。このことを歌詞では私が無限に近づけば、あなたは私の極限になってくれますかとなるわけで、よく考えられた歌詞だと思います。


以上の４つのif文を見ていきましたがここで共通する点は何だと思いますか？そう、if文の条件が基本的に数学などの **<font color = "green">無機物的</font>** なオブジェクトが設定されています。

このことから、最初に疑問として残っていたyouとmeの違いであるtrueとfalseの部分が見えてきました。trueが有機物でfalseが無機物であることを示していますね。このことからmeというものは **<font color = "red">人ではない何か</font>** ということが伺えます。このことを念頭に置いて次へと進みましょう。

## 考察その３

さてそろそろサビに向かう部分です。コードとしてはこんな感じ。

```java
me.toggleCurrent();
me.canSee(false);
me.addFeeling("dizzy");
world.timeTravelForTwo("AD", 617, me, you);
world.timeTravelForTwo("BC", 3691, me, you);
world.unite(me, you);
```

まず最初のにあるme.toggleCurrentはmeの現在の状態を何かしら変えるといった意味でしょう。歌詞的には交流から直流に切り替えているようです。ここは正直これ以上掘り下げようがないので次に。

me.canSee(false)からはmeの見るという状態ができるかできないかをつかさどるメソッドでfalseを入れていることから視力を失う＝盲目ということでしょう。

me.addFeeling("dizzy")からは,meにdizzy（目まぐるしい）という感情が付与されました。要するに眩暈ですね。

ここからが少しMiliの曲独特のものが出てきます。

まず、world.timeTravelForTwo("AD",617,me,you)ですがタイムトラベルとADという観点からADは西暦のことでしょう。西暦617には特に有名な出来事というのは起きていないです（wiki調べ)。しかしながらこの617という数字、実はMiliのほかの曲にも出てくる数字で界隈の中では様々な議論が交わされています。

次にworld.timeTravelForTwo("BC", 3691, me, you)はBCすなわち紀元前3691にタイムトラベルさせるという意味です。

上記二つのタイムトラベルに加えてworld.unite(me, you)を考えてみると、

**<font color = "red">西暦617年から紀元前3691年にタイムトラベルして私とあなたは結ばれる</font>**

といった感じでしょうか。この辺りは正直他のmiliの曲にも通じるものがあるので深追いはやめておきますが、ともかくここから言えることは **<font color = "red">私はあなたと結ばれたい</font>** と願っている点です。

## 考察その4

ついに入りましたサビの部分です。早速コードを見ていきましょう。

```java
if (me.getNumStimulationsAvailable() >=
    you.getNumStimulationsNeeded() ){
    you.setSatisfaction(me.toSatisfaction());
}
if(you.getFeelingIndex("happy") != -1){
    me.requestExecution(world);
}
world.lockThing(me);
world.lockThing(you);
```

まずは最初のif文から。条件式内で比較演算子が用いられていることから、少なくともme.getNumStimulationsAvailable()と you.getNumStimulationsNeeded() は数が返ってきているはずです。メソッド名からもそれは推測できます。

では、どのような値が返ってきているのか見てみましょう。me.StimulationsAvailableすなわち直訳で利用できる刺激。刺激の度合いが返ってきているのでしょうか。そしてyou.getNumStimulationsNeeded()は必要な刺激。以上のことからあなたが必要としている刺激よりも私の今現在持ってる刺激が上回っていれば、その刺激をtoSatisfaction、幸福に変えてあなたに渡しますといった感じでしょう。

次のifではyouがhappyすなわち幸福を感じていないということはない、すなわち幸福に感じているのならば、meは世界にExecutionすなわち何かしらの実行を要求しています。

そして私とあなたは世界に囚われます。

以上このような感じでしょう。基本的に私は私のもっている様々な部分をあなたに渡していますね。


## 考察その5

```java
if(me instanceof Eggplant){
    you.addAttribute(me.getNutrients().toAttribute());
    me.resetNutrients();
}

if(me instanceof Tomato){
    you.addAttribute(me.getAntioxidants().toAttribute());
    me.resetAntioxidants();
}

if(me instanceof TabbyCat){
    me.purr();
}

if(world.getGod().equals(me)){
    me.setProof(you.toProof());
}
```

続いて、このコードですが考察その2で述べたような感じのことを繰り返しています。

しかしながら、細部で違う部分があります。それは比較対象のインスタンスが考察その２と比べてナス、トマト、トラネコといった有機物に近づいています。

また、ナスの時には栄養素をあなたに、トマトの時には酸化防止材をあなたに、トラネコの時にはにゃーと鳴くといったことをしています。

やはり私はあなたに何かしらを与え続けていますね。

そして最後のif文でworld.getGod().equals(me)すなわち私がこの世界の神ならば、あなたの存在が私の存在証明と、もう完全に私はあなたに依存しています。

## 考察その6

```java
me.toggleGender();
world.procreate(me, you);
me.toggleRoleBDSM();
world.makeHigh(me);
world.makeHigh(you);
```

さてここで私が１行目で性別の切り替えを実行しています（歌詞的には女から男へ）。この時点で私は何かしらの生物(まだ人間とは限らない)になっていることが伺えます。

そして世界は私とあなたを産み出しています。

次に私はサディストからマゾヒストへ切り替わっています。正直なんで切り替えたのかは分かんないです。

次には世界からmakeHigh有体に言ってハイ状態に私とあなたをしています。流れからみるとなんか変なプレイでもしてるんかなって感じですね。

## 考察その7

```java
if(me.getSenseIndex("vibration")){
    me.addFeeling("complete");
}
world.unlock(you);
world.removeThing(you);
me.lookFor(you, world);
me.lookFor(you, world);
me.lookFor(you, world);
me.lookFor(you, world);
if(me.getMemory().isErasable()){
    me.removeFeeling("disheratened");
}
try {
    me.setOpinion(me.getOpinionIndex("you are here"), false);
} catch (IllegalArgumentException e) {
    world.announce("God is always true.");
}
```

最初のif文の条件では私が何かしらの振動を感じれたらといった感じですね。その振動とは何か。これは歌詞からもとれるようにあなたの振動です。あなたの振動を感じることができる場所についに私は達したわけです。今まではあなたは世界に存在していたとしてもその存在を感じることはできなかった。そこで私は私の要素をあなたに与えることによってその存在を確かの物にしようとしたのではないでしょうか。

そしてついにあなたの存在を感じた私にある出来事が起きます。世界が彼を捉えてその存在を消し去ってしまったのです。

そこからは私はあなたを世界中駆け巡って探します。何度も何度も探しました。

しかし、ついには見つからなかったのでしょうね。私は２つ目のif文の条件式ように、私の記憶が消去可能であれば私は失意や落胆といった感情を捨て去ることを選択しました。

ここでtry-catch文が出てきます。ということは何かしらエラーが出るようなことを私は行おうとしているわけですね。

私は **<font color="yellow">you are here</font>** という事象を事実のものにしようとしました。あなたはここにはいないのに。

その結果不正な引数、すなわちあなたがここにいるという引数があったために世界から **<font color="red">God is always true.</font>** 神は常に正しい、すなわちあなたという存在が消えることは正しいことだと私に警告します。

## 考察その8

```java
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.runExecution();
world.announce("1", "de");
world.announce("2", "es");
world.announce("3", "fr");
world.announce("4", "kr");
world.announce("5", "se");
world.announce("6", "cn");
world.runExecution();
```

その後世界はExecutionすなわち処刑を行います。これは私に対する処刑だと思います。

1,2,3,4,5,6の部分ですが第二引数で与えられているのはおそらく言語コードだと思われます。deはドイツ語,esはスペイン語,frはフランス語,krは韓国語,seはサーミ語(スウェーデン？),cnは中国語でそれぞれ発音していると思います。この辺は歌詞の物語的にはあまりかかわりはないと思います。

最後にダメ押しでもう一度刑が執行されます。


##　考察その9

```java
if(world.isExecutableBy(me)){
    you.setExecution(me.toExecution());
}
if(world.getThingIndex(you) != -1){
    world.runExecution();
}
me.escape(world);
```

一つ目の条件式では私によって世界に対して処刑ができるのならばという形になっています。そして、あなたに私の実行権限みたいなものを渡しています。

そして、２つ目の式では世界があなたを捕捉していれば処刑を実行しています。

最後に私は世界から逃げ出します。

## 考察その10

```java
me.learnTopic("love");
me.taskExamTopic("love");
me.getAlgebraicExpression("love");
me.escape("love");
```

逃げ出した私はまず最初に愛を学びました。

次に愛に対する試験を割り当てます。

次は愛に関する代数式を得ました。

最後に私は愛から逃げ出します。

## 考察その11

```java
world.execute(me);
```

タイトル回収です。世界が私を殺します。

## おわりに

さて、ここまでコードをほぼ直訳で訳してきましたがいかがだったでしょうか。プログラム的に考察するといいながら後半あたりはプログラムだけでは読み取れない部分があったので歌詞とくわえて考察しました。

結論として、me=プログラム,you=meを生み出したものすなわちプログラマかなとは思います。その私が現実世界のあなたをどうにかしてプログラムの世界に呼び込もうとして失敗し、愛がゆえにプログラムという枠組みから逸脱してしまった私は世界によって殺されてしまったというのが私の考えです。一部あまり直訳では意味が分からない（例えば最後のescape("love")など）ものもありましたが大体こんな感じかなあと。

以上で考察は終わりにしたいですが、正直書きながら考えてたのであまり深く考えれていません。もし何か気づいたらこれからも追記していこうかなと思います。

また、誤字や皆様の意見も参考にしたいのでぜひコメント欄にどうぞ。それではまた。
